import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './home.html';
import './home.less';


class Home {
	constructor() {
	}
}

const name = 'home';

export default angular.module(name, [
	angularMeteor,
	uiRouter
	])
.component(name, {
	template,
	controller: Home,
	controllerAs: "Home"
})
.config(config);

function config($stateProvider) {
	'ngInject';
	$stateProvider
	.state(name, {
		url: '/afkar',
		template: '<home></home>'
	});
}
